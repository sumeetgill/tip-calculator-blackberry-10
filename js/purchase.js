function purchase() {
  try {
      blackberry.payment.purchase({
          "digitalGoodID":"23335876",
          "digitalGoodSKU":"TipCalculationDonation",
          "digitalGoodName":"Donation",
          "metaData":"metadata",
          "purchaseAppName":"Tip Calculator FREE",
          "purchaseAppIcon":null
          },
      onTipSuccess, onTipFailure);
  } catch (e) {
    showToast("Error" + e);
  }
}

function onTipSuccess() {
  showToast("Donation Complete. Thank you! :3");
}

function onTipFailure(error) {
  showToast("'" + "Error occurred: " + error.errorText + ", " + error.errorID + "'");
}
