// By Sumeet Gill
// Tip Calculator
// Version 2.0

showToast = function(message) {
    blackberry.ui.toast.show(message);
};

// ************** Defaults **************

// loads defaults to the form if there are any
loadDefaults = function(){
	// declarations
	var subTotalDefault = localStorage['subTotal'];
	var taxPercentageDefault = localStorage['taxPercentage'];
	var tipPercentageDefault = localStorage['tipPercentage'];
	var tipAmountDefault = localStorage['tipAmount'];
	var splitAmountDefault = localStorage['splitAmount'];
		
	// load defaults into textboxes
	if (subTotalDefault != null){
		document.getElementById('subTotal').value = subTotalDefault;
	};	

	if (taxPercentageDefault != null){
		document.getElementById('taxPercentage').value = taxPercentageDefault;
	};

	if (tipPercentageDefault != null){
		document.getElementById('tipPercentage').value = tipPercentageDefault;
	};

	if (tipAmountDefault != null){
		document.getElementById('tipAmount').value = tipAmountDefault;
	};

	if (splitAmountDefault != null){
		document.getElementById('splitAmount').value = splitAmountDefault;
	};
};

// saves defaults to appear on startup
saveDefaults = function(){
	var subTotal = document.getElementById('subTotalDefault').value;
	var taxPercentage = document.getElementById('taxPercentageDefault').value;
	var tipPercentage = document.getElementById('tipPercentageDefault').value;
	var tipAmount = document.getElementById('tipAmountDefault').value;
	var splitAmount = document.getElementById('splitAmountDefault').value;
	
	// save default for subTotal if not blank
	if (subTotal != ""){
		localStorage['subTotal'] = subTotal;
	};	
	
	// save default for tax Percentage if not blank
	if (taxPercentage != ""){
		localStorage['taxPercentage'] = taxPercentage;
	};
	
	// save default for tipPercentage if not blank
	if (tipPercentage != ""){
		localStorage['tipPercentage'] = tipPercentage;
	};
	
	// save default for tipAmount if not blank
	if (tipAmount != ""){
		localStorage['tipAmount'] = tipAmount;
	};
	
	// save default for splitAmount if not blank
	if (splitAmount != ""){
		localStorage['splitAmount'] = splitAmount;
	};

	// takes focus off focused element to make virtual keyboard hide
	var activeElement = document.activeElement;
	activeElement.blur();

	showToast("Favourites Saved.");
};

// ************** Tip Calculations **************

calculateTip = function(){
	// declaring variables
	var subTotal =  Number(document.getElementById('subTotal').value);
	var taxPercentage = Number(document.getElementById('taxPercentage').value);
	var tipPercentage = Number(document.getElementById('tipPercentage').value);
	var tipAmount = Number(document.getElementById('tipAmount').value);
	var splitAmount = Number(document.getElementById('splitAmount').value);

	var output = document.getElementById('output');
	var buttonOutput = document.getElementById('shareButton');
	var calculation = 0;
	var taxTotal = 0;
	var grandTotal = 0;
	var validation = true;

	// takes focus off focused element to make virtual keyboard hide
	var activeElement = document.activeElement;
	activeElement.blur();

	//bbm stuff
	var message = "";
	var button = document.createElement('div');

	// validation
	if (subTotal == ""){
		showToast('Please enter Subtotal.');
		validation = false;
	} else if (tipAmount == "" && tipPercentage == ""){ // error checking for blank in tip amount and percentage
		showToast('Please enter either "Tip (%)" or "Tip Amount ($)"');
		validation = false;
	} else if (tipAmount != "" && tipPercentage != ""){ // error checking for having both tip amount and percentage
		showToast('Cannot have both "Tip (%)" and "Tip Amount ($)". Please delete one.');
		validation = false;
	};

	// tip and split calculations
	if (validation){
		//changes to output tab and highlights it
		selectOutput();
		// changes highlighting of action bar to output
		var tab = document.getElementById('outputBar'),
		actionBar = document.getElementById('actionBar');
		actionBar.setSelectedTab(tab);

		var items = [], item;
		// defaults split amount to 1 if not set
		if (splitAmount == ""){
			splitAmount = 1;
		};
		
		// does the tax calculation if not blank
		if (taxPercentage != ""){
			taxTotal = (subTotal * (taxPercentage / 100));
		};
		
		// subtotal
		item = document.createElement('div');
		item.setAttribute('data-bb-type','item');
		item.setAttribute('data-bb-title','Subtotal');
		item.innerHTML = '$' + subTotal;
		items.push(item);
		// calculates tip amount
		if (tipAmount == ""){
			calculation = (subTotal * (tipPercentage / 100));
			grandTotal = (calculation + subTotal + taxTotal);

			// Tip Amount
			item = document.createElement('div');
			item.setAttribute('data-bb-type','item');
			item.setAttribute('data-bb-title','Tip Amount');
			item.innerHTML = '$' + calculation.toFixed(2) + "  |   $" + (calculation/splitAmount).toFixed(2) + ' each';
			items.push(item);


			// set to local storage for sharing feature
			localStorage['message'] = "Subtotal: $" + subTotal + "\n"
				+ "Tip Amount: $" + calculation.toFixed(2) + ' ($' + (calculation/splitAmount).toFixed(2) + '/ea).' + "\n"
				+ "Tax Amount: $" + taxTotal.toFixed(2) + "\n"
				+ "Cost Per Person: $" + (grandTotal / splitAmount).toFixed(2) + "\n"
				+ "Grand Total: $" + grandTotal.toFixed(2) + "\n"
				+ "\n"
				+ "Download Tip Calculator Free on BlackBerry World:" + "\n" 
        		+ 'http://appworld.blackberry.com/webstore/content/20080647/';
		};
		
		// calculates tip percentage
		if (tipPercentage == ""){
			calculation = ((tipAmount / subTotal) * 100);
			grandTotal = ((((calculation / 100) + 1) * subTotal) + taxTotal);

			//tip percentage
			item = document.createElement('div');
			item.setAttribute('data-bb-type','item');
			item.setAttribute('data-bb-title','Tip Percentage');
			item.innerHTML =  calculation.toFixed(2) + '%';
			items.push(item);

			// set to local storage for sharing feature
			localStorage['message'] = "Subtotal: $" + subTotal + "\n"
				+ "Tip Percent: " + calculation.toFixed(2) + "%" + "\n"
				+ "Tax Amount: $" + taxTotal.toFixed(2) + "\n"
				+ "Cost Per Person: $" + (grandTotal / splitAmount).toFixed(2) + "\n"
				+ "Grand Total: $" + grandTotal.toFixed(2) + "\n"
				+ "\n"
				+ "Download Tip Calculator Free on BlackBerry World:" + "\n" 
        		+ 'http://appworld.blackberry.com/webstore/content/20080647/';
		};

		// tax amount
		item = document.createElement('div');
		item.setAttribute('data-bb-type','item');
		item.setAttribute('data-bb-title','Tax Amount');
		item.innerHTML = '$' + taxTotal.toFixed(2);
		items.push(item);

		//Cost per person
		item = document.createElement('div');
		item.setAttribute('data-bb-type','item');
		item.setAttribute('data-bb-title','Cost Per Person');
		item.innerHTML = '$' + (grandTotal / splitAmount).toFixed(2);
		items.push(item);

		//grand total
		item = document.createElement('div');
		item.setAttribute('data-bb-type','item');
		item.setAttribute('data-bb-title','Grand Total');
		item.innerHTML = '$' + grandTotal.toFixed(2);
		items.push(item);

		document.getElementById('output').innerHTML = "";
		document.getElementById('outputList').refresh(items);

		// share button
		buttonOutput.innerHTML = "";
		button.setAttribute('data-bb-type','button');
		button.setAttribute('onclick', 'shareResults("Share your Tip Calculation");');
		button.innerHTML = "Share Calculation"; // inner text
		button = bb.button.style(button); // Apply our styling
		buttonOutput.appendChild(button);
		bb.refresh();
	};
};

// ************** Clearing **************

// clears everything from form
clearForm = function(){
	document.getElementById('subTotal').value = "";
	document.getElementById('taxPercentage').value = "";
	document.getElementById('tipPercentage').value = "";
	document.getElementById('tipAmount').value= "";
	document.getElementById('splitAmount').value = "";
};

// clears local storage (defaults)
clearDefaults = function(){
	localStorage.removeItem('subTotal');
	localStorage.removeItem('taxPercentage');
	localStorage.removeItem('tipPercentage');
	localStorage.removeItem('tipAmount');
	localStorage.removeItem('splitAmount');
	showToast("Favourites cleared.");
};

// ************** Navigation **************

selectAbout = function() {
    document.getElementById('Settings').style.display = 'none';
    document.getElementById('Main').style.display = 'none';
	document.getElementById('About').style.display = 'block';
};

selectOutput = function() {
	document.getElementById('About').style.display = 'none';
    document.getElementById('Main').style.display = 'none';
    document.getElementById('Settings').style.display = 'block';
};

selectMain = function() {
    document.getElementById('Settings').style.display = 'none';
    document.getElementById('About').style.display = 'none';
    document.getElementById('Main').style.display = 'block';
};

displayAbout = function() {
	document.getElementById("about").innerHTML = blackberry.app.version;
};

// ************** Sharing **************

shareResults = function(message){
    var sharedMessage = localStorage['message'];  		
	var request = {
        action : 'bb.action.SHARE',
        mime : 'text/plain',
        data : sharedMessage,
        target_type: ["VIEWER", "CARD"]
    };

	blackberry.invoke.card.invokeTargetPicker(request, message,
	// success
	function() {},

	// error
	function(e) {
		console.log(e);
	});
};

shareDownload = function(){
	localStorage['message'] =  "Download Tip Calculator Free on BlackBerry World!" + "\n" 
        		+ 'http://appworld.blackberry.com/webstore/content/20080647/';
	shareResults("Invite Friends to download");
};

openWebLink = function() {
    blackberry.invoke.invoke({
        target: "sys.appworld",
        action: "bb.action.OPEN",
        uri: "appworld://content/20080647"
    }, function() {
		console.log("Success");
	}, function(e) {
		console.log(e);
	});
};
