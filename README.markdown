Welcome
============

I created this Tip Calculator as a "get your feet wet" project, so I could get more familiar with mobile development and the BlackBerry 10 environment.

Rights
------------
You're free to use this software for non-commercial usage. Please do not submit it to any app store.


App Store
------------
This app has been downloaded close to 10,000 times and has 30 five-star ratings.

[Check out this app in BlackBerry world.](https://appworld.blackberry.com/webstore/content/20080647/)

Images
------------
Here is what the latest version of the app looks like:

![Main Tab](https://i.imgur.com/4RQHVTbl.png "Main Tab")
![Output Tab](https://i.imgur.com/MTD9ExBl.png "Output Tab")
![About Tab](https://i.imgur.com/MUYZRskl.png "About Tab")
![Side Menu](https://i.imgur.com/Lwl1Y9gl.png "Side Menu")